\section{Das Maximum-Likelihood-Prinzip}

Wir wollen nun einen Schätzer für T konstruieren. Die Idee ist, dass
wir jenes $\upsilon$ wählen, dass den Daten die höchste Wahrscheinlichkeit gibt.

\begin{defn}
  Sei $\modelX$ ein Standardmodell mit Referenzmaß $\nu$ und $(\Theta, \mathcal{G})$ ein Messraum.
  sei $\rho_\upsilon$ die Dichte von $\mathbb{P}_\upsilon$ bezüglich $\nu$.
  \begin{enumerate}
    \item Die Funktion 
      \begin{align*}
        \rho \colon \mathcal{X} \times \Theta &\to [0, \infty) \\
        \rho(x, \upsilon) &= \rho_\upsilon(x)
      \end{align*}
      heißt \emph{Likelihood-Funktion}.
    \item Ein Schätzer $T \colon \mathcal{X} \to \Theta$ heißt \emph{Maximum-Likelihood-Schätzer} (MLE) falls
      \begin{align*}
        \rho(x, T(x)) = \max_{\upsilon \in \Theta} \rho(x, \upsilon) \: \forall x \in \mathcal{X}
      \end{align*}
      Häufige Bezeichnung: $T = \mle$.
  \end{enumerate}
\end{defn}

\begin{defn} Die Funktion $l(x, \upsilon) = \log{\rho(x, \upsilon)}$ heißt \emph{Log-Likelihood-Funktion}.
\end{defn}

\begin{rem} Es gilt:
  \begin{align*}
    l(x, \mle(x)) = \max_{\upsilon \in \Theta} l(x, \upsilon) \: \forall x \in \mathcal{X}
  \end{align*}
  Es reicht also die log-Likelihood-Funktion zu betrachten um
  $\mle$ zu bestimmen.
\end{rem}

\section{Die Momentenmethode}

Nun schätzen wir $\upsilon$ so, daß die theoretischen Momente von $Q_\upsilon$ mit den empirischen Momenten
der Stichprobe $(x_1, \dots, x_n)$ übereinstimmen.

Sei $\model{\mathbb{R}}{\mathcal{B}(\mathbb{R})}{Q}$ ein statistisches Modell und $r \in \mathbb{N}$.
Zu jedem $k \in \{1, \dots, r\}$ existiere das $k$-te Moment von $Q_\upsilon$, i.e.
\begin{align*}
  m_k(\upsilon) \coloneqq \int \! x^k Q_\upsilon(dx) < \infty \: \forall \upsilon \in \Theta
\end{align*}
sei $g : \mathbb{R}^r \to \mathbb{R}$ stetig und $\tau(\upsilon) = g(m_1(\upsilon), \dots, m_r(\upsilon))$ eine Kenngröße. Für $n \in \mathbb{N}$
betrachten wir das $n$-fache Produktmodell $\model{\mathbb{R}^n}{\mathcal{B}(\mathbb{R^n})}{Q^{\otimes n}}$.
Jede Stichprobe $(x_1, \dots, x_n) = x$ induziert eine \emph{empirische Verteilung}:
\begin{align*}
  \rho_n[x](A) \coloneqq \frac{1}{n}\sum\limits_{i=1}^n \delta_{x_i}(A) \:, \forall A \in \mathcal{B}(\mathbb{R})
\end{align*}
Das $k$-te Stichprobenmoment von $x$ ist dann das $k$-te moment von $\rho_n[x]$:
\begin{align*}
  \mu_k[x] \coloneqq \int \! y^k \, d\rho_n[x](y) = \frac{1}{n} \sum\limits_{i=1}^n x_i^k
\end{align*}

\paragraph{Momentenmethode}

Schätze $\upsilon$ durch $T(x)$, so dass 
\begin{align*}
  m_1(T(x)) = \mu_1[x], \dots, m_r(T(x)) = \mu_r[x]
\end{align*}
Dies induziert einen Schätzer $\hat{\tau}$  für $\tau(\upsilon)$ durch
\begin{align*}
  \hat{\tau} \coloneqq \tau(T(x)) = g(\mu_1[x], \dots, \mu_r[x])
\end{align*}

\begin{rem}
  Beachte: $r$ muss groß genug sein, damit das resultierende Gleichungssystem nach $\upsilon$ lösbar ist.
\end{rem}

\lec
